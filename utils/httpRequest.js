let bind_url = 'https://liar.yuejiyou.com/bind'
let skill_url = 'https://liar.yuejiyou.com/skill'
let fight_url = 'https://liar.yuejiyou.com/beginFight'
let ready_url = 'https://liar.yuejiyou.com/ready'

function httpRequest(url, data) {
  const requestTask = wx.request({
    url: url,
    data: data,
    header: {
      'content-type': 'application/json'
    },
    method: 'GET',
    success: function (res) {
      console.log(url + ' response is' + res.data)
    },
    fail: function (res) {
      console.log(url + ' response is' + res.data)
    }
  })
}

module.exports = {
  httpRequest: httpRequest,
  bind_url: bind_url,
  skill_url: skill_url,
  fight_url: fight_url,
  ready_url: ready_url
}