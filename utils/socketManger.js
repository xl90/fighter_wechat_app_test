const httpRequest = require('httpRequest.js')
var mangeApp = getApp()
var socket_url = 'ws://sw.yuejiyou.com:8282'

// recvice msg type
let socketTyepConfig = {
    init:'init',
    skill:'skill',
    start:'start'
}

var socketOpen = false
var socketMsgQueue = []

const connection = url =>{
  wx.connectSocket({
    url: socket_url,
  })
}

wx.onSocketOpen(function (res) {
  socketOpen = true
  for (var i = 0; i < socketMsgQueue.length; i++) {
    sendSocketMessage(socketMsgQueue[i])
  }
  socketMsgQueue = []
})

function sendSocketMessage(msg) {
  if (socketOpen) {
    wx.sendSocketMessage({
      data: msg
    })
  } else {
    socketMsgQueue.push(msg)
  }
}

wx.onSocketMessage(function(res){
  var pages = getCurrentPages()
  var currentPage = pages[pages.length - 1]

  var obj = JSON.parse(res.data)
  if (socketTyepConfig.init == obj.type){
    mangeApp.globalData.client_id = obj.client_id
    var paramData = {
      'client_id': obj.client_id,
      'uid': '11'
    }
    httpRequest.httpRequest(httpRequest.bind_url, paramData)
  }else if (socketTyepConfig.skill == obj.type){
    console.log(res.data)
  }else if (socketTyepConfig.start == obj.type){
    currentPage.setData({
      userInfo:obj.userInfo
    })
    console.log(res.data)
  }
})

module.exports = {
  connection: connection,
}
